<?php
require_once get_template_directory().'/core/ThemeBuilder.php';

function get_theme_builder(){
    return ThemeBuilder::create_builder();
}

function init_theme(){
    $builder = get_theme_builder();
    $builder->init_theme();
    $builder->add_bootstrap_shortcode();
    $builder->create_option_page();
}
add_action('after_setup_theme','init_theme');


function get_theme_logo(){
    $builder = get_theme_builder();
    return $builder->get_theme_logo();
}

function import_theme_styles(){
    $style_list = array(
        'main-style' => array(
            'src' => 'casinoevents.css'
        ),
    );
    $builder = get_theme_builder();
    $builder->import_google_fonts(array('Open+Sans:400,600'));
    $builder->import_assets_style($style_list);
}
add_action("wp_enqueue_scripts","import_theme_styles");

function import_theme_scripts(){
    $script_list = array(
        'vendor-js' => array(
            'src' => 'casinoevents-vendor.js'
        ),
        'main-js' => array(
            'src' => 'casinoevents.js'
        ),
        
    );
    $builder = get_theme_builder();
    $builder->import_assets_script($script_list);
}
add_action("wp_enqueue_scripts","import_theme_scripts");

function get_page_slider_data(){
   $builder = get_theme_builder();
   return $builder->get_slider_data('website_slider','slider',array('link'));
}

function has_sidebar_widget(){
    $builder = get_theme_builder();
    return $builder->has_sidbar_widget();
}

function body_classes( $classes ) {
    // Adds a class of custom-background-image to sites with a custom background image.
    if ( get_background_image() ) {
        $classes[] = 'custom-background-image';
    }

    // Adds a class of group-blog to sites with more than 1 published author.
    if ( is_multi_author() ) {
        $classes[] = 'group-blog';
    }

    // Adds a class of no-sidebar to sites without active sidebar.
   
    if ( is_active_sidebar( 'sidebar-1' ) ) {
        $classes[] = 'has-sidebar';
    } else {
        $classes[] = 'no-sidebar';
    }

    // Adds a class of hfeed to non-singular pages.
    if ( ! is_singular() ) {
        $classes[] = 'hfeed';
    }

    return $classes;
}
add_filter( 'body_class', 'body_classes' );

function my_mce4_options($init) {
    $custom_colours = '
        "f1831d", "Brand Primary",
        "2eafb0", "Secondary",
        "2e2e2e", "Text Color",
    ';
    $init['textcolor_map'] = '['.$custom_colours.']';
    $init['textcolor_rows'] = 1;
    return $init;
}
add_filter('tiny_mce_before_init', 'my_mce4_options');
function wp_editor_fontsize_filter( $buttons ) {
        array_shift( $buttons );
        array_unshift( $buttons, 'fontsizeselect');
        return $buttons;
}    
add_filter('mce_buttons_2', 'wp_editor_fontsize_filter');
if ( ! function_exists( 'dc_mce_text_sizes' ) ) {
	function dc_mce_text_sizes( $initArray ){
		$initArray['fontsize_formats'] = "9px 10px 12px 14px 16px 18px 20px 24px 28px 32px 36px";
		return $initArray;
	}
}
add_filter( 'tiny_mce_before_init', 'dc_mce_text_sizes' );