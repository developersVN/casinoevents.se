<?php
/**
 * The sidebar containing the main widget area
 *
 */

if (is_active_sidebar( 'sidebar-1' )  ) : ?>
	 <aside class="col-xs-12 col-sm-5 col-md-4 casinoevents-sidebar">
		<div id="widget-area" class="widget-area" >
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
		</div><!-- .widget-area -->
	</aside><!-- .secondary -->

<?php endif; ?>
