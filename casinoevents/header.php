<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php $header_image = get_header_image(); ?>
    <header style="background: url(<?php echo $header_image ?>) center center no-repeat; background-size: cover;" class="casinoevents-header" id="casinoevents-header">
        <nav class="navbar">
          <div class="container">
            <div class="rows">
                <div class="navbar-header">
                    <button class="navbar-toggle" id="menu-button" data-toggle="collapse" data-target="#casinoevents-menu-nav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span><span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand logo" href="<?php echo home_url();?>">
                        <img src="<?php echo get_theme_logo(); ?>" alt="<?php bloginfo('name'); ?>" />
                 </a>
                </div>
                <div class="collapse navbar-collapse" id="casinoevents-menu-nav">
                    <?php
                        wp_nav_menu( array(
                            'menu' => 'top_menu',
                            'theme_location' => 'primary',
                            'depth' => 2,
                            'container' => false,
                            'menu_class' => 'nav navbar-nav',
                            'walker' => new wp_bootstrap_navwalker())
                        );
                    ?>
                </div>
            </div>
          </div>
        </nav>
        
    </header>
    <div class="casinoevents-search">
        <div class="container">
            <?php get_search_form(); ?>
        </div>
    </div>
    <div class="container casinoevents-base-container">
            